# Godot_learning

Just a simple repo of me learning how to use the Godot game engine.

The tutorial that I am following is located here:
https://www.youtube.com/playlist?list=PLda3VoSoc_TSBBOBYwcmlamF1UrjVtccZ

I did not make the tutorial, the tutorial is available on YouTube by "BornCG" and it is an excellent tutorial that I recommend you watch. It was designed by BornCG to teach others how to use the Godot Engine by making a simple game. Since BornCG made the tutorial publically available for free, I put an open source apache 2.0 license on my edits, which will allow you to pretty much do anything you want with my edits and variant of the game. I am not intending to sell or market this game, I just wanted to be able to reference my work and have a sharable version of my finished project.

I did make several edits to my version of the game, as I learned newer ways to do things, or to change the asthetics.

- Edited the rotation speed to be the degrees of radius based off of vector speed, which allowed the ball to look like it slowly rolls to a stop when you release the keys.
- Lots of color changes.
- A different player ball.
- A different brick mesh library.
- etc....
