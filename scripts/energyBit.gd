extends Area

signal energyTaken

func _ready():
	pass 

func _physics_process(delta):
	rotate_y(deg2rad(1))


func _on_energyBit_body_entered(body):
	if body.name == "SphereMan":
		emit_signal("energyTaken")
		$AnimationPlayer.play("CollectedAnimation")
		$Timer.start()


func _on_Timer_timeout():
	queue_free()
