extends KinematicBody

var velocity = Vector3(0,0,0)
const SPEED = 6

func _ready():
	pass

func _physics_process(delta):
	if Input.is_action_pressed("ui_left") and Input.is_action_pressed("ui_right"):
		velocity.x = 0
	elif Input.is_action_pressed("ui_right"):
		velocity.x = SPEED
		$MeshInstance.rotate_z(deg2rad(-velocity.x))
	elif Input.is_action_pressed("ui_left"):
		velocity.x = -SPEED
		$MeshInstance.rotate_z(deg2rad(-velocity.x))
	else:
		velocity.x = lerp(velocity.x,0,0.1)
		$MeshInstance.rotate_z(deg2rad(-velocity.x))
	
	if Input.is_action_pressed("ui_up") and Input.is_action_pressed("ui_down"):
		velocity.z = 0
	elif Input.is_action_pressed("ui_up"):
		velocity.z = -SPEED
		$MeshInstance.rotate_x(deg2rad(velocity.z))
	elif Input.is_action_pressed("ui_down"):
		velocity.z = SPEED
		$MeshInstance.rotate_x(deg2rad(velocity.z))
	else:
		velocity.z = lerp(velocity.z,0,0.1)
		$MeshInstance.rotate_x(deg2rad(velocity.z))
		
	move_and_slide(velocity)


func _on_ring_body_entered(body):
	if body.name == "SphereMan":
		$AnimationPlayer.play("GameOverAnimation")
		$Timer.start()
		


func _on_Timer_timeout():
	get_tree().change_scene("res://gameOver.tscn")
