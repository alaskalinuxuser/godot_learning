extends Label

var energyCollected = 0

func _ready():
	text = String(energyCollected)

func _on_energyBit_energyTaken():
	energyCollected = energyCollected + 1
	_ready()
	if energyCollected == 43:
		$Timer.start()

func _on_Timer_timeout():
	get_tree().change_scene("res://winScreen.tscn")
